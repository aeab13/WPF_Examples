﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WordCutre
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool modificat=false;
        String arxiu="";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void mnuOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Rich Text Format (*.rtf)|*.rtf|All files (*.*)|*.*";
            if (dlg.ShowDialog() == true)
            {
                FileStream fileStream = new FileStream(dlg.FileName, FileMode.Open);
                TextRange range = new TextRange(rtbEditor.Document.ContentStart, rtbEditor.Document.ContentEnd);
                range.Load(fileStream, DataFormats.Rtf);
            }
        }

        private void rtbEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            modificat = true;
        }
        private void cmdIncreaseZoom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            rtbEditor.FontSize += 10;
            tbZoom.Text = "Zoom: " + (int)rtbEditor.FontSize;
        }

        private void cmdIncreaseZoom_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = rtbEditor != null && rtbEditor.FontSize >= 52 ? false : true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (modificat)
            {
                e.Cancel = true;
                var resultat = MessageBox.Show("Vols guardar el fitxer?", "Guardar", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (resultat)
                {
                    case MessageBoxResult.Cancel:
                        break;
                    case MessageBoxResult.Yes:
                        Guardar();
                        e.Cancel = false;
                        break;
                    case MessageBoxResult.No:
                        e.Cancel = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private void cmdDecreaseZoom_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = slZoom != null && slZoom.Value<= 1 ? false : true;

        }

        private void cmdDecreaseZoom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            slZoom.Value--;
            tbZoom.Text = "Zoom: " + Math.Round(slZoom.Value*10,0)+"%";
        }

        private void cmdGuardarFitxerCom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            GuardarCom();
        }

        private void cmdGuardarFitxer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                Guardar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar fitxer", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Guardar()
        {
            if (arxiu == "")
            {
                GuardarCom();
            }
            else {
                FileStream fileStream = new FileStream(arxiu, FileMode.Create);
                TextRange range = new TextRange(rtbEditor.Document.ContentStart, rtbEditor.Document.ContentEnd);
                range.Save(fileStream, DataFormats.Rtf);
                modificat = false;
            }
        }
        private void GuardarCom()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Rich Text Format (*.rtf)|*.rtf|All files (*.*)|*.*";
            if (sfd.ShowDialog() == true)
            {
                arxiu = sfd.FileName;
                FileStream fileStream = new FileStream(sfd.FileName, FileMode.Create);
                TextRange range = new TextRange(rtbEditor.Document.ContentStart, rtbEditor.Document.ContentEnd);
                range.Save(fileStream, DataFormats.Rtf);
                modificat = false;
            }
        }

        private void cmdNou_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (modificat)
            {
                var resultat = MessageBox.Show("Vols guardar el fitxer?", "Guardar", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (resultat)
                {
                    case MessageBoxResult.Cancel:
                        break;
                    case MessageBoxResult.Yes:
                        Guardar();
                        rtbEditor.Document.Blocks.Clear();
                        break;
                    case MessageBoxResult.No:
                        rtbEditor.Document.Blocks.Clear();
                        modificat = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private void cmdClose_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (modificat)
            {
                var resultat = MessageBox.Show("Vols guardar el fitxer?", "Guardar", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (resultat)
                {
                    case MessageBoxResult.Cancel:
                        break;
                    case MessageBoxResult.Yes:
                        Guardar();
                        Close();
                        break;
                    case MessageBoxResult.No:
                        Close();
                        break;
                    default:
                        break;
                }
            }
            else
                Close();
        }
    }
}
