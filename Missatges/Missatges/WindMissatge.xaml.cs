﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Missatges
{
    /// <summary>
    /// Lógica de interacción para WindMissatge.xaml
    /// </summary>
    public partial class WindMissatge : Window
    {
        string resposta;
        string[] possiblesRepostes;
        public WindMissatge():this("Sense títol", "Sense missatge")
        {
            InitializeComponent();
        }
        public WindMissatge(string titol, string missatge)
        {
            InitializeComponent();
            Titol = titol;
            Missatge = missatge;
        }

        public string[] PossiblesRespostes
        {
            get { return possiblesRepostes; }
            set { possiblesRepostes = value;
            Button boto;
            foreach (string resposta in possiblesRepostes)
            {
                boto = new Button();
                boto.Content = resposta;
                boto.Click += btnGeneric_Click;
                stkBotons.Children.Add(boto);
            }
            }
        }

        public string Resposta
        {
            get { return resposta; }
        }

        public string Titol
        {
            get { return this.Title; }
            set { this.Title = value; }
        }

        public string Missatge
        {
            get { return tbMissatge.Text; }
            set { tbMissatge.Text = value; }
        }

        private void btnGeneric_Click(object sender, RoutedEventArgs e)
        {
            Button boto = (Button)sender;
            resposta = boto.Content.ToString();
            this.Close();
        }

    }
}
