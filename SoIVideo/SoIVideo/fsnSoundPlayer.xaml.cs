﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SoIVideo
{
    /// <summary>
    /// Lógica de interacción para fsnSoundPlayer.xaml
    /// </summary>
    public partial class fsnSoundPlayer : Window
    {
        SoundPlayer reproductor;
        MediaPlayer reproductorMP;
        public fsnSoundPlayer()
        {
            InitializeComponent();
            reproductorMP = new MediaPlayer();
        }

        private void btnReprodueixAudio_Click(object sender, RoutedEventArgs e)
        {
            reproductor = new SoundPlayer();
            reproductor.SoundLocation = System.IO.Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, @"media\test.wav");
            try
            {
                reproductor.Load();
                reproductor.Play();
            }
            catch (FileNotFoundException fnfEx)
            {
                Title = fnfEx.Message;
            }
            catch (FormatException err)
            {
                Title = err.Message;
            }
        }

        private void btnReprodueixAudioSincronament_Click(object sender, RoutedEventArgs e)
        {
            reproductor = new SoundPlayer();
            reproductor.SoundLocation = System.IO.Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, @"media\test.wav");
            try
            {
                reproductor.Load();
                reproductor.PlaySync();
            }
            catch (FileNotFoundException fnfEx)
            {
                Title = fnfEx.Message;
            }
            catch (FormatException err)
            {
                Title = err.Message;
            }
        }

        private void btnAtura_Click(object sender, RoutedEventArgs e)
        {
            if (reproductor != null)
                reproductor.Stop();
        }

        private void btnReprodueixAudioMediaPlayer_Click(object sender, RoutedEventArgs e)
        {
            reproductorMP.Open(
                new Uri("media/test.mp3", UriKind.Relative));
            reproductorMP.Play();
        }
    }
}
