﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Crono
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer crono;
        TimeSpan transcorregut;

        public MainWindow()
        {
            InitializeComponent();
            crono = new DispatcherTimer();
            crono.Interval = TimeSpan.FromSeconds(0.1);
            crono.Tick += crono_Tick;
            tbCrono.MouseDown += tbCrono_MouseDown;
        }

        void tbCrono_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (crono.IsEnabled)
                crono.Stop();
            else
                crono.Start();
        }

        void crono_Tick(object sender, EventArgs e)
        {
            transcorregut = transcorregut.Add(crono.Interval);
            tbCrono.Text = String.Format("{0:00}:{1:00}:{2:0}", transcorregut.Minutes, transcorregut.Seconds, transcorregut.Milliseconds/100);
        }
    }
}
