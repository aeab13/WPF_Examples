﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Esdeveniments
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double coordX, coordY;
        Point origen;
        public MainWindow()
        {
            InitializeComponent();
            CreaBoto(stkDreta);
            CreaBoto(stkEsquerre);
        }
        private void CreaBoto(Panel panell)
        {
            Button boto = new Button();
            boto.FontSize = 20;
            int nBoto = panell.Children.Count+1;
            boto.Content = nBoto.ToString();
            boto.HorizontalAlignment = HorizontalAlignment.Stretch;
            panell.Children.Add(boto);
            boto.MouseDown += Stk_MouseDown;
            boto.MouseMove += Stk_MouseMove;
         /*   if (panell == stkDreta)
                boto.Click += BotoSuma_Click;
            else
                boto.Click += BotoResta_Click;*/
        }
        private void BotoSuma_Click(object sender, RoutedEventArgs e)
        {
            if (pgbMarcador.Value == pgbMarcador.Maximum)
                tbMissatges.Text = "S'ha assolit el màxim";
            else
            {
                pgbMarcador.Value++;
                tbMissatges.Text = "El valor és: " + pgbMarcador.Value;
            }
        }
        
        private void BotoResta_Click(object sender, RoutedEventArgs e)
        {
            if (pgbMarcador.Value == pgbMarcador.Minimum) tbMissatges.Text = "S'ha assolit el mínim.";
            else
            {
                pgbMarcador.Value--;
                tbMissatges.Text = "El valor és: " + pgbMarcador.Value;
            }
        }

        private void DockPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            Panel panell;
            if ((e.KeyboardDevice.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                panell = stkDreta;
            else
                panell = stkEsquerre;
            if ((e.Key == Key.OemPlus) || (e.Key == Key.Add))
            {
                CreaBoto(panell);
                Button boto = (Button)panell.Children[panell.Children.Count - 1];
                int nCops = int.Parse(boto.Content.ToString()) - 1;
                for (int i = 0; i < nCops; i++)
                {
                    if (panell == stkDreta)
                        boto.Click += BotoSuma_Click;
                    else
                        boto.Click += BotoResta_Click;
                }
                e.Handled = true;
            }
            else if (e.Key == Key.OemMinus)
            {
                if (panell.Children.Count > 0)
                    panell.Children.RemoveAt(panell.Children.Count - 1);
                e.Handled = true;
            }
        }



        private void txtEntrada_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!"0123456789".Contains(e.Text))
                e.Handled = true;
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            tbMissatges.Text = "Coordenades: (" + e.GetPosition(cnvPaper).X + ", " + e.GetPosition(cnvPaper).Y + ")";
            if (e.LeftButton == MouseButtonState.Pressed || e.RightButton== MouseButtonState.Pressed)
            {
                Line linia = new Line();
                linia.Stroke = e.LeftButton == MouseButtonState.Pressed ? Brushes.Blue : Brushes.White;
                linia.StrokeThickness = 5;
                linia.X1 = coordX;
                linia.Y1 = coordY;
                coordX = e.GetPosition(cnvPaper).X;
                coordY = e.GetPosition(cnvPaper).Y;
                linia.X2 = coordX;
                linia.Y2 = coordY;
                cnvPaper.Children.Add(linia);

                Ellipse cercle = new Ellipse();
                cercle.Stroke = e.LeftButton == MouseButtonState.Pressed ? Brushes.White : Brushes.Blue;
                cercle.Width = 50;
                cercle.Height = 50;
                cnvPaper.Children.Add(cercle);
                Canvas.SetLeft(cercle, coordX - cercle.Width / 2);
                Canvas.SetTop(cercle, coordY - cercle.Height / 2);
            }
        }

        private void cnvPaper_MouseDown(object sender, MouseButtonEventArgs e)
        {
            coordX = e.GetPosition(cnvPaper).X;
            coordY = e.GetPosition(cnvPaper).Y;
        }

        private void Stk_MouseMove(object sender, MouseEventArgs e)
        {
            Point posicio = e.GetPosition(null);
            Vector distancia = origen - posicio;

            if (e.LeftButton == MouseButtonState.Pressed && (Math.Abs(distancia.X) > SystemParameters.MinimumHorizontalDragDistance || Math.Abs(distancia.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                Panel panell = (Panel)sender;
                if (panell.Children.Count > 0)
                {
                    Button boto = (Button)panell.Children[panell.Children.Count - 1];
                    tbMissatges.Text = "M'he endut el botó: " + boto.Content.ToString();
                    DataObject arrosegament = new DataObject(DataFormats.Text, boto.Content.ToString());
                    DragDrop.DoDragDrop(panell, arrosegament, DragDropEffects.Copy | DragDropEffects.Move);
                }
            }
        }

        private void Stk_MouseDown(object sender, MouseButtonEventArgs e)
        {
            origen = e.GetPosition(null);
        }

        private void STK_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
                e.Effects = DragDropEffects.Copy | DragDropEffects.Move;
            else
                e.Effects = DragDropEffects.None;
        }

        private void STK_Drop(object sender, DragEventArgs e)
        {
            Panel desti=(Panel)sender;
            Panel origen=sender==stkDreta?stkEsquerre:stkDreta;
            if (e.Effects == DragDropEffects.Copy)
                CreaBoto(desti);
            else
            {
                CreaBoto(desti);
                origen.Children.RemoveAt(origen.Children.Count - 1);
            }
        }
    }
}